<?php

use Illuminate\Database\Seeder;

use App\Product as Product;

class ProductTableSeeder extends Seeder {

	public function run() {

		DB::table('products')->delete();

		// Create products
		Product::create(array(
			'name' => 'Pizza',
			'description' => 'Decision tree for ordering a pizza: 1) Are you hungry? 2) Yes, order a pizza or 3) No, order one anyway because you will get hungry soon.',
			'price' => 11.99,
		));

		Product::create(array(
			'name' => 'Throwing Stars',
			'description' => 'Are you a ninja?  It doesn\'t matter... get throwing stars!',
			'price' => 49.99,
		));

	}
}