<?php

use Illuminate\Database\Seeder;

use App\User as User;

class UserTableSeeder extends Seeder {

	public function run() {

		DB::table('users')->delete();

		// Create users
		User::create(array(
			'name' => 'Steven',
			'email' => 'steven@voltagead.com',
			'password' => bcrypt('test'),
		));

		User::create(array(
			'name' => 'Randy',
			'email' => 'randy@voltagead.com',
			'password' => bcrypt('test'),
		));


	}
}