<?php namespace App\Http\Controllers;

use App\Product as Product;

use Illuminate\Http\Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return view('home', [
			'products' => $products,
		]);
	}


	public function purchase(Request $request, $itemID)
	{
		// Validate the input
		self::validateOrder($request);

		try {

			$product = Product::find($itemID);
			$user = \Auth::user();
			
			$user->charge($product->price * 100, [
				'source' => \Input::get('stripeToken'),
				'receipt_email' => \Input::get('email'),
			]);

			return view('complete', [
				'product' => $product,
			]);

		} catch (\Exception $e) {
			return 'Error: ' . $e->getMessage();
		}
	}

	private function validateOrder($request) {

		// Validate the input
		$this->validate($request, [
			'number' => array('required', 'numeric'),
			'exp_month' => array('required', 'digits:2'),
			'exp_year' => array('required', 'digits:4'),
			'cvc' => array('required', 'numeric'),
			'email' => array('required', 'email'),
		]);

	}



}
