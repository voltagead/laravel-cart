<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Everything in this group requires authentication
Route::group(['middleware' => 'auth'], function() {

	// Home
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');

	// Purchase complete
	Route::post('{id}', 'HomeController@purchase');
});