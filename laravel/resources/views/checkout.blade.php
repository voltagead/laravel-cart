						<div class="form-group">
							<label class="col-md-4 control-label">CC Number</label>
							<div class="col-md-6">
								<input type="name" class="form-control" name="number" data-stripe="number" value="{{ Input::old('number') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Card Expiration Month (MM)</label>
							<div class="col-md-6">
								<input type="name" class="form-control" name="exp_month" data-stripe="exp-month" value="{{ Input::old('exp_month') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Card Expiration Year (YYYY)</label>
							<div class="col-md-6">
								<input type="name" class="form-control" name="exp_year" data-stripe="exp-year" value="{{ Input::old('exp_year') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">CVC</label>
							<div class="col-md-6">
								<input type="name" class="form-control" name="cvc" data-stripe="cvc" value="{{ Input::old('cvc') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Email Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ Input::old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-sm-offset-4">
								<input type="submit" class="purchase" value="Buy it!">
							</div>
						</div>



