@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div>
			<div class="panel panel-default">
				<div class="panel-heading">My Store</div>

				<div class="panel-body">

					<h1>Purchase Complete!</h1>
					<p>Thank you for your order. <a href="/">Click here</a> to continue shopping.</p>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
