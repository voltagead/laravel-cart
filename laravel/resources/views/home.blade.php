@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div>
			<div class="panel panel-default">
				<div class="panel-heading">My Store</div>

				<div class="panel-body">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@foreach ($products as $product)
						<div class="product">
							<h2>{{ $product->name }}</h2>
							<p>{{ $product->description }}</p>
							<p>${{ number_format($product->price, 2, '.', ',') }}</p>
							<form class="form-horizontal buy" role="form" method="POST" action="{{ action('HomeController@purchase', [$product->id]) }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">

								@include('checkout')
							</form>
						</div>
					@endforeach

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		Stripe.setPublishableKey('pk_test_CxDxwcfeD6zpfuRIMDT109S7');

		var $form; // Form that is being submitted
		
		var stripeResponseHandler = function(status, response) {

			if (response.error) {
				console.log('error');
				$form.find('.payment-errors').text(response.error.message);
				// $form.find('button').prop('disabled', false);
			} else {
				console.log('success');
				var token = response.id;
				$form.append($('<input type="hidden" name="stripeToken" />').val(token));
				$form.get(0).submit();
			}
		};

		$('.buy').submit(function(event) {
			$form = $(this);
			Stripe.card.createToken($form, stripeResponseHandler);
			return false;
		});
	});

</script>

@endsection
